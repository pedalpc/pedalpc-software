import asyncio
from pathlib import Path
import logging
import config
from string import Template
import shutil
import sys

logger = logging.getLogger(__name__)

#def hash_password(salt, email, password):
#    import hashlib
#    h = hashlib.sha256()
#    h.update(salt)
#    h.update(email)
#    h.update(password)
#    return h.hexdigest()

async def create_battery_svg(data):
    ''' generate SVG based on battery level and battery current '''
    FULL_WIDTH  = 300
    FULL_START  = 21
    BORDER      = 4
    html = Template('''
<svg xmlns="http://www.w3.org/2000/svg" id="battery-level" viewBox="0 0 400 200" height="200px" width="400px">
    <rect y="75" x="$CATHODE_START" height="50" width="16" id="cathode" style="fill:$color;fill-opacity:1" />
    <rect y="25" x="$FULL_START_STR" height="150" width="$FULL_WIDTH_STR" id="full-region" style="fill:$color;fill-opacity:1" />
    <rect y="29" x="$empty_start" height="142" width="$empty_width" id="empty-region" style="fill:#ffffff;fill-opacity:1" />
    <text id="percent-text" y="136" x="173" style="font-size:96px;line-height:1;font-family:sans-serif;text-align:center;text-anchor:middle;fill:$text_color;fill-opacity:1;stroke:none">
         $battery_percent</text>
</svg>
''')
    try:
        level = data['battery_level']
        html = html.substitute(
            FULL_WIDTH_STR  = str( FULL_WIDTH + 2 * BORDER),
            FULL_START_STR  = str( FULL_START ),
            CATHODE_START   = str( FULL_START + FULL_WIDTH + 2 * BORDER ),
            battery_percent = "%d%%" % (100 * level),
            empty_start     = "%d" % ( BORDER + FULL_START + (FULL_WIDTH * level) ),
            empty_width     = "%d" % ( FULL_WIDTH * (1 - level) ),
            color = '#00FF00' if data['amps']['battery'] > 0.5 else '#FF7777',
            text_color = '#666666'
        )
    except (KeyError, TypeError, ValueError):
        level =  0
        html = html.substitute(
            FULL_WIDTH_STR  = str( FULL_WIDTH + 2 * BORDER),
            FULL_START_STR  = str( FULL_START ),
            battery_percent = "NA",
            empty_start     = "%d" % ( BORDER + FULL_START + (FULL_WIDTH * level) ),
            empty_width     = "%d" % ( FULL_WIDTH * (1 - level) ),
            color = "#999999",
            text_color = "#999999"
        )
    error = ""
    if not 'BATTERY_SVG_PATH' in dir(config) or not config.BATTERY_SVG_PATH: 
        error = "BATTERY_SVG_PATH is not defined in local_config.py"
    else:
        path = Path(config.BATTERY_SVG_PATH)
        if not path.exists():
            error = f"{config.BATTERY_SVG_PATH} does not exist in filesystem"
    if error:
        logger.error(f"Cannot create SVG battery icon: {error}")
        return
    file = open(path, "w")
    file.write(html)
    file.close()
    # File is saved with owner and group set to "root" if script is run as root user.
    # This sets them to that of it's parent directory ("jim" & "www-data", usually).
    owner = path.parent.owner()
    group = path.parent.group()
    shutil.chown(path, user=owner, group=group)
    return html

# test this from home directory like this (note no `.py` extension!):
#   $ python3 -m pedalpc_server.modules.extra`
if __name__ == "__main__":
    logger.addHandler(logging.StreamHandler(sys.stdout))
    data = {"battery_level":.25, "amps":{"battery":2}}
#    data = {"battery_level":.605, "amps":{"battery":0.5}}
#    data = {"battery_level":.05, "amps":{"battery":1}}
#    data = {}
#    data = {"battery_level":.605}
    asyncio.run(create_battery_svg(data))

