#! /usr/bin/python3
import os
from subprocess import check_output, run, CalledProcessError
import logging

#TODO: 
# 1) if eth0 is in use, set up AP on wlan0; otherwise, use ap0
# 2) allow IP subnet to be changed 

# use same logger name as server script to log to same file
log = logging.getLogger('server')

wifi_iface = "wlan0"
ap_iface = "ap0"
wired_iface = "eth0"

def get_ssids_and_channels(iface=wifi_iface):
    '''
    Returns a list of wifi access points and their channels, sorted by
    decreasing signal strength.
    '''
    # perform a scan first to update results
    run(f"wpa_cli -i {iface} scan", shell=True, capture_output=False) 
    # generate list, converting freq to wifi channels & sort by signal strength
    # note: braces are doubled due to being an f-string, see:
    # https://www.python.org/dev/peps/pep-0498/#escape-sequences
    scan_results = check_output(
        f"wpa_cli -i {iface} scan_results | \
            awk 'BEGIN {{FS=\"\t\"; OFS=\"\t\"}} \
            /:/{{channel = ($2<2472) ? ($2-2407)/5 : ( \
                ($2<5745) ? 32+($2-5160)/5 : 149+($2-5745)/5 \
                ); print  $3,channel,$5}}' | sort -rn", 
                shell=True, text=True)
    ap_list = []
    for ap in scan_results.split('\n'):
        # split fields into signal strength, channel, and SSID (if it exists)
        d = ap.split('\t')
        # skip if SSID is blank
        if len(d) == 3 and d[2] != '':
            ap_list.append(dict(ssid=d[2], channel=d[1]))
    return ap_list

def get_current_ssid(iface=wifi_iface):
    ''' 
    returns ssid of current wifi or hotspot connection, if it exists 
    '''
    try:
        return check_output(
            f"iw {iface} info |  awk '/ssid /{{print $2}}'",
            shell=True, text=True).strip()
    except:
        return None

def validate_ssid(ssid):
    ''' validate an SSID string and return error message if invalid '''
    # The SSID can be up to 32 alphanumeric characters
    if len(ssid) > 31:
        return 'SSID is too long'
    if ssid[0] in '!#;':
        return "1st character in SSID cannot be !, # or ;"
    if any(s in '#+]/"\t' for s in ssid):
        return '# +, ], /, ", TAB, are invalid SSID characters'
    return None

def validate_psk(password):
    ''' validate an WPA-PSK passphrase and return error message if invalid '''
    # password must be between 8 and 63 characters in the
    # range of 32 to 126, decimal
    if len(password) < 8 or len(password) > 63:
        return 'password must be between 8 and 63 characters'
    if any((ord(s)<32 or ord(s)>126) for s in password):
        return 'invalid characters in password'

def get_wifi_access_points(iface=wifi_iface):
    '''
    returns a dict of available APs, with the active one set to 'True'
    '''
    ssids = {} 
    try:
        # `iw <interface> info` returns current state of a wifi interface 
        # in `<tab><key><space><value>\n` format
        active_ssid = get_current_ssid(iface)
        for d in get_ssids_and_channels(iface):
            if d['ssid'] == active_ssid:
                ssids[d['ssid']] = True
            else:
                ssids[d['ssid']] = False
    except:
        # rescanning will sometimes throw an error
        pass
    #log.debug(ssids)
    return ssids 

def get_connections(active=False):
    ''' returns a list of all network connections '''
    command = "ip -br -o addr | grep -v '^lo '"
    if active:
        command += "| grep UP"
    command += " | awk '{print $1}'"
    return check_output(command, shell=True, text=True).split('\n')[:-1]

def get_active_connection():
    '''
    get connection currently connected to the internet
    should be either 'wired' or 'wireless'
    '''
    # use route since both wifi and wired connections may both be 
    # 'connected' but only one is transmitting data
    iface = check_output(
        "ip route get 1.1.1.1 | awk '{print $5}'",
        shell=True, text=True).strip()
    if iface in get_connections(active=True):
        return iface
    else:
        return None

def get_ap_password():
    ''' returns current access point password with [\n"] stripped '''
    return check_output(
        "awk '{FS=\"=\"}/wpa_passphrase/{print $2}' /etc/hostapd/hostapd.conf",
        shell=True, text=True).strip('"').strip()

def get_ip4_octets(iface):
    ''' returns an interface's current ip address as a list of octets '''
    return check_output(
        f"ip -o -br -4 addr | awk '/{iface}/{{print $3}}' | cut -d/ -f1",
        shell=True, text=True).strip().split('.')

def is_hotspot_in_use(ip_address):
    ''' 
    Returns True if the client is connected via access point, not LAN.
    Test is performed by comparing the first three octets of the client's IP 
        address to that of the access point.
    '''
    client_ip_range = '.'.join(ip_address.split('.')[:-1])
    hotspot_ip_range = check_output(
        "ip -o -br -4 addr | grep {ap_iface} | \
        awk '{print $3}' |  cut -d'.' -f1-3", 
        shell=True, text=True).strip()
    return (client_ip_range == hotspot_ip_range)

def connect_wifi(ssid='', password='', iface=wifi_iface):
    '''
    connect iface to router
    Returns a message if successful or an error message if not
    '''
    log.debug(
        f"starting connection to {ssid} using {iface}")
    try:
        # ssid is needed, but password might be blank (open-access wifi)
        if not ssid: 
            return "need SSID of access point to connect to"
        file = "/etc/wpa_supplicant/wpa_supplicant.conf"
        # wpa_supplicant is launched by dhcpcd
        run(f"systemctl stop dhcpcd.service", shell=True, check=True)
        # update wpa_supplicant info 
        run(f"sed -Ein 's/(.*ssid=).*/\\1\"{ssid}\"/' {file}", shell=True, check=True)
        run(f"sed -Ein 's/(.*psk=).*/\\1\"{password}\"/' {file}", shell=True, 
                check=True)
        run(f"systemctl start dhcpcd.service", shell=True, check=True) 
        return None
    except Exception as err:
        log.exception(f"could not connect to '{ssid}': {err}")
        return f"Could not connect to '{ssid}'.  Please try again."

def update_ap_ip4(ipv4_3='1'):
    ''' updates the third octet in the AP's IPv4 address '''
    if int(ipv4_3)<0 or int(ipv4_3)>255:
        return "IPv4 address octet must be an integer in the range of 0-255"
    try:
        range_pat = '(dhcp-range=([0-9]+.){2})[0-9]+(.[0-9]+,([0-9]+.){2})[0-9]+'
        run(f"sed -i -E 's/{range_pat}/\\1{ipv4_3}\\3{ipv4_3}/' /etc/dnsmasq.conf",
            shell=True, check=True, text=True)
        option_pat = '(dhcp-option=3,([0-9]+.){2})[0-9]+'
        run(f"sed -i -E 's/{option_pat}/\\1{ipv4_3}/' /etc/dnsmasq.conf",
            shell=True, check=True, text=True)
        dhcpcd_pat = '(static ip_address=([0-9]+.){2})[0-9]+'
        run( f"sed -i -E '/interface {ap_iface}/,+3 s/{dhcpcd_pat}/\\1{ipv4_3}/' \
            /etc/dhcpcd.conf", shell=True, check=True, text=True)
        dnsmasq_pat = '^(address=\/.*\/([0-9]+.){2})[0-9]+'
        run( f"sed -i -E 's/{dnsmasq_pat}/\\1{ipv4_3}/' \
            /etc/dnsmasq.conf", shell=True, check=True, text=True)
        return None
    except Exception as err:
        log.exception(f"unable to update AP IPv4 address using 3rd octet of {ipv4_3}: {err}")
        return f"Could not update access point IPv4 address!"
            
def start_ap(ssid='', password=''):
    '''
    start up a wireless access point
    assumes user is already connected to server either via wifi or ethernet
    '''
    try:
        if not ssid:
            return 'SSID cannot be blank'
        # get wifi band and channel of current connection...
        channel = check_output(
            f"iw {wifi_iface} info | awk '/\\tchannel /{{print $2}}'", 
            shell=True, text=True).strip()
        # ... or that of the strongest signal, if unavailable
        if not channel:
            channel = get_ssids_and_channels()[0]['channel']
        if int(channel) > 14:
            band = "a"
        else:
            band = "g"
        # update hostapd.conf before restarting
        file="/etc/hostapd/hostapd.conf"
        run(f"sed -E -i 's/(.*channel=).*/\\1{channel}/' {file}", shell=True, check=True)
        run(f"sed -E -i 's/(.*hw_mode=).*/\\1{band}/' {file}", shell=True, check=True)
        run(f"sed -E -i 's/(.*ssid=).*/\\1{ssid}/' {file}", shell=True, check=True)
        run(f"sed -E -i 's/(.*wpa_passphrase=).*/\\1{password}/' {file}", 
                shell=True, check=True)
        run(
            "systemctl stop dhcpcd.service && systemctl restart hostapd.service \
            && systemctl start dhcpcd.service", shell=True, check=True)
        return None
    except Exception as err:
        log.exception(f"could not connect to '{ssid}': {err}")
        return f"Could not start access point using '{ssid}'.  Please try again."

def stop_ap():
    ''' stop access point '''
    try:
        run("systemctl stop hostapd.service", shell=True, check=True)
        return None 
    except Exception as err:
        return f"could not stop AP: {err}"
