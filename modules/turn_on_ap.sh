if [[ $# != '2' ]]; then
	echo "usage: sudo $0 <SSID> <PASSPHRASE>"
	exit 1
	
fi
SSID=$1
PASSWORD=$2
WPA_PSK="$(wpa_passphrase $SSID $PASSWORD | awk -F= '/	psk/ { print $2 }')"
WIRELESS_ADAPTER="$( awk -F: '/unmanaged-devices/ {print $2}' /etc/NetworkManager/conf.d/10-ignore-interfaces.conf)"
echo $WIRELESS_ADAPTER
# this may be down already anyway, but just in case...
ifdown $WIRELESS_ADAPTER 2> /dev/null
sed -i "s/^\(ssid=\).*/\1$SSID/" /etc/hostapd.conf
sed -i "s/^\(wpa_passphrase=\).*/\1$PASSWORD/" /etc/hostapd.conf
sed -i "s/^\(wpa_psk=\).*/\1$WPA_PSK/" /etc/hostapd.conf 
sed -i "s/^#\(unmanaged-devices=\)/\1/" /etc/NetworkManager/conf.d/10-ignore-interfaces.conf
sed -i "s/^DAEMON_CONF=.*/DAEMON_CONF=\/etc\/hostapd.conf/" /etc/init.d/hostapd
sed -i "s/^#\(iptables-restore \)/\1/" /etc/rc.local
sed -i "s/^#\(service dnsmasq\)/\1/" /etc/rc.local
mv /etc/network/armbian.ap.nat /etc/network/interfaces.d/
sleep 2
#ifup $WIRELESS_ADAPTER 2> /dev/null
ifup $WIRELESS_ADAPTER 

## reload services
systemctl daemon-reload
service hostapd stop
rm -f /var/run/hostapd/*
sleep 1 
#service hostapd start; sleep 1 
service rc.local restart
sleep 1
# dnsmasq is started by rc.local
#service dnsmasq start; sleep 1
service network-manager restart
sleep 5
# hostapd won't work unless it is started last
service hostapd start
systemctl restart systemd-resolved.service
