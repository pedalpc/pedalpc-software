WIRELESS_ADAPTER="$( awk -F: '/unmanaged-devices/ {print $2}' /etc/NetworkManager/conf.d/10-ignore-interfaces.conf)"
echo $WIRELESS_ADAPTER
sed -i "s/^DAEMON_CONF=.*/DAEMON_CONF=/" /etc/init.d/hostapd
#disable DNS
systemctl daemon-reload
systemctl disable dnsmasq.service >/dev/null 2>&1
ifdown $WIRELESS_ADAPTER 2> /dev/null
#rm -f /etc/network/interfaces.d/armbian.ap.nat
mv /etc/network/interfaces.d/armbian.ap.nat /etc/network
#  bridge is not used on an Orange Pi PC Plus
#rm -f /etc/network/interfaces.d/armbian.ap.bridge
rm -f /var/run/hostapd/* >/dev/null 2>&1
sed -i "s/^iptables/#&/" /etc/rc.local
sed -i "s/^service dnsmasq/#&/" /etc/rc.local
sed -i "s/^unmanaged-devices=/#&/" /etc/NetworkManager/conf.d/10-ignore-interfaces.conf
iptables -F
## reload services
systemctl daemon-reload
service network-manager stop; sleep 1 
service hostapd stop; rm -f /var/run/hostapd/*; sleep 1 
service dnsmasq stop; sleep 1
service network-manager start; sleep 5;
systemctl restart systemd-resolved.service
