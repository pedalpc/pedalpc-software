#! /usr/bin/python3
##! /home/jim/Projects/venv/aiohttp/bin/python3 
# use full path to Python3 if installed in virtual env
# otherwise /usr/bin/Python3 will work

# change to working directory to read/write to database successfully
# (otherwise will get 'no table named user' error')
import os,sys
os.chdir(sys.path[0])

# using Python3 native urllib gave 403 server errors
# so used Requests library instead
#from urllib.parse import urlencode
#from urllib.request import Request, urlopen
#from urllib.error import URLError, HTTPError
import sqlite3
import logging
import requests

logging.basicConfig(level=logging.INFO,
    filename = 'upload_data.log',
    format='%(asctime)s-%(module)s-%(lineno)d-%(levelname)s: %(message)s')
    
database = 'pedalpc.sqlite'
url = 'https://www.pedalpc.com/log'

# get data from database
conn = sqlite3.connect(database)
conn.row_factory = sqlite3.Row
cur = conn.cursor()
data = cur.execute(
    "SELECT * FROM power_data WHERE date = date('now','localtime')").fetchone()
conn.close()

# use requests library
post_fields = {
    'email': 'jim@pedalpc.com',
    'minutes': data['minutes'], 
    'generator': f"{data['watthrs']:.3f}"
    }
r = requests.post(url, data=post_fields)
logging.info(f'sending: {str(post_fields)}; server response: {r.status_code}, {r.reason}')

# using Python3 native urllib gave 403 server errors
#data = urlencode(post_fields)
#data = data.encode('ascii')
#try:
#    print('posting: ', post_fields)
#    print('data is: ', data)
#    req = Request(url, data)
#    resp = urlopen(req)
## HTTPError must come first!
#except HTTPError as e:
#    print('server could not fulfill request:', e.code)
#    print(e.info())
#except URLError as e:
#    print('could not reach server: ', e.reason)
#else:
#    print('***everything is fine****')
#    print(resp.read())


