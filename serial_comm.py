import sys
import glob
import serial
import logging
import time
import datetime
import json

# use same logger name as server script to log to same file
# log = logging.getLogger('server')

DEVICE_COUNT = 9  # of power sockets (including fan header)
# power sockets supporting PWM mode (last one is fan header)
PWM_DEVICES = [3, 4, 5, 6, DEVICE_COUNT]

# modem settings
# SERIAL_PORT = '/dev/ttyS3'      # Orange Pi 2
# SERIAL_PORT = '/dev/ttyS0'      # Raspberry Pi 3
# SERIAL_PORT = '/dev/ttyS1'      # Odroid C2
# SERIAL_PORT = "/dev/ttyS1"      # Orange Pi Zero & Zero 3
# SERIAL_PORT = "/dev/ttyAML6"    # Libre Computer Sweet Potato
SERIAL_PORT = "/dev/ttyS5"  # Orange Pi Zero 2W (using uart5)
# SERIAL_BPS      = 115200                # serial port data rate in bps
SERIAL_BPS = 9600  # serial port data rate in bps
SERIAL_TIMEOUT = 1

# PCB and firmware settings
ADC_RESOLUTION = 4096.0  # 1024(10-bit ADC) or 4096 (12-bit ADC)
ADC_VREF = 2.048  # Vref setting for ADC
SENSE_RESISTOR = 0.04  # resistance of current sense resistor
VOLTAGE_RATIO = 1.0 / (33.0 + 1.0)  # resistors used in voltage divider
GAIN = 1.0  # gain of op amp prior to ADC


def list_serial_ports():
    """Lists serial ports

    :raises EnvironmentError:
        On unsupported or unknown platforms
    :returns:
        A list of available serial ports
    """
    if sys.platform.startswith("win"):
        # TODO: check power calculations
        ports = ["COM" + str(i + 1) for i in range(16)]  # range can be 256
    elif sys.platform.startswith("linux") or sys.platform.startswith(
        "cygwin"
    ):
        # this is to exclude your current terminal "/dev/tty"
        ports = (
            glob.glob("/dev/ttyACM*")
            + glob.glob("/dev/ttyUSB*")
            + glob.glob("/dev/ttyS*")
        )
    elif sys.platform.startswith("darwin"):
        ports = glob.glob("/dev/tty.*") + glob.glob("/dev/cu*")
    else:
        raise EnvironmentError("Unsupported platform")
    return ports


def check_serial_port(port=SERIAL_PORT):
    """
    check if selected serial port matches data format of PedalPC
    """
    logging.debug("trying port " + port + "...")
    try:
        conn = serial.Serial(port, SERIAL_BPS, timeout=SERIAL_TIMEOUT)
        # send command it's ready to go and receive data
        for x in "G\r\n":
            conn.write(x.encode("ascii"))
            time.sleep(0.2)
        logging.debug(conn.readline())
        attempts = 3
        while attempts > 0:
            raw_data = conn.readline().decode("ascii")
            logging.debug("raw_data =" + raw_data)
            if parse_data(raw_data):  # returns a list of lists if successful
                return conn
            attempts -= 1
        return False
    except:
        logging.exception("could not connect to serial port")
        return False


def serial_init():
    """get serial port name and open serial port connection"""
    serial_con = None
    # serial port is unknown when user's computer is connecting directly to the
    # board over USB (it depends on their operating system)
    # serial_port = ''
    # c,con = connect_to_db()
    # c.execute('SELECT value FROM setting WHERE key = "serial_port"')
    # serial_port = c.fetchone()[0]
    # serial port is known when using serial port on Raspberry Pi or Orange Pi
    serial_port = SERIAL_PORT
    if serial_port:
        serial_con = check_serial_port(serial_port)
        # works OK->save serial port as cookie and redirect to dashboard
    if not serial_con:
        serial_port = ""
        # get list of available serial ports for Mac/Linux, all COM port
        # names in Windows
        ports = list_serial_ports()
        logging.debug("ports = ", ports)
        for p in ports:
            serial_con = check_serial_port(p)
            if serial_con:
                serial_port = p
                break
        # check if any returns valid data
        # works OK->save serial port as cookie and redirect to dashboard
    return (serial_port, serial_con)


def set_timeout(amount):
    """
    set when battery relay in control box opens after pedalling ceases
    """
    # incoming value is a string, so need to parse to int
    try:
        amount = int(amount)
        # set a minimum threshold value for shutdown time
        if amount < 90:
            amount = 90
    except ValueError:  # not an integer!
        return None
    logging.info(f"setting timeout to {amount}")
    return f"T{amount}\r\n"


def change_device(device_num, new_state):
    """change device state to 'on', 'auto_off', 'pwm_mode' (PWM) or 'off'"""
    commands = {"on": "U", "auto_off": "A", "pwm_mode": "P", "off": "D"}
    if int(device_num) in range(1, DEVICE_COUNT + 1) and (
        new_state.lower() in commands
    ):
        # send command to microcontroller to set [device_num] to [new_state]
        logging.info("turning device #%s '%s'" % (device_num, new_state))
        return f"{commands[new_state]}{device_num}\r\n"
    else:
        logging.error(f"invalid data: {device_num}, {new_state}")
        return None


def change_duty_cycle(device_num, value):
    """
    change the duty cycle on a 12V socket or fan
    """
    try:
        value = int(value)
        # set a minimum threshold value for shutdown time
        if value < 0:
            value = 0
        elif value > 256:
            value = 256
    except ValueError:  # not an integer!
        return None
    logging.info(f"setting duty cycle on socket #{device_num} to {value}")
    return f"%{device_num}{value}\r\n"


def parse_data(serial_data):
    """
    parses the data returned from a read command (conn.write("R"))
    format is <adc_data>;<time_data>;<device_status>;<duty_cycle>;<battery_level>|<response>|<debugging data>
    values within each section are separated by commas
    adc_data has battery voltage, battery amps, and each device's amps
    time_data has battery draining time and shutdown limit time
    device_status has on/off status for generator relay + each device
    duty_cycle has current duty cycle for each PWM-capable socket (0-255)
    response is the string returned from the microcontroller after a command
    debugging data are any values desired from the microcontroller (calibration values, etc.)
    """
    try:
        (data, response, debug_values) = serial_data.split("|")
        logging.debug(
            f"parse_data: data:{data}, response:{response}, debug_values:{debug_values}"
        )
        (
            adc_data,
            time_data,
            device_status,
            duty_cycle,
            battery_level,
        ) = data.split(";")
        logging.debug(
            f"adc_data:{adc_data}  time_data:{time_data}  device_status:{device_status}  duty_cycle:{duty_cycle}  battery_level:{battery_level}"
        )
        if len(adc_data.split(",")) == DEVICE_COUNT + 2:
            if len(time_data.split(",")) == 2:
                if len(device_status.split(",")) == DEVICE_COUNT + 1:
                    if len(duty_cycle.split(",")) == len(PWM_DEVICES):
                        if len(battery_level.split(",")) == 2:
                            return (
                                adc_data.split(","),
                                time_data.split(","),
                                device_status.split(","),
                                duty_cycle.split(","),
                                battery_level.split(","),
                                response,
                            )
        return None
    except:
        return None


def convert_raw_to_dict(
    adc_data, time_data, device_status, duty_cycle, battery_level, response
):
    """
    converts parsed data to dict, for later converting to JSON
    returns adc data, socket_settings, time data, battery level, duty cycle, pid constants, and response as dict of dict
    """
    # possible states of a power socket and its corresponding numerical code
    socket_status_codes = {
        "0": "off",
        "1": "on",
        "2": "auto_off",
        "3": "pwm_mode",
    }
    import traceback

    device_list = [
        "battery",
    ]  # keys used when returning json amps and W
    pwm_device_list = []
    for i in range(1, DEVICE_COUNT):
        device_list.append("socket_" + str(i))
        if i in PWM_DEVICES and i != DEVICE_COUNT:
            pwm_device_list.append("socket_" + str(i))
    device_list.append("fan")
    pwm_device_list.append("fan")
    try:
        volts = float(adc_data[0]) / ADC_RESOLUTION / VOLTAGE_RATIO * ADC_VREF
        json_data = {
            "amps": {},
            "watts": {},
            "socket_settings": [],
            "duty_cycle": {},
        }
        total_load = 0
        g_adc = 0
        val = 0
        # TODO: check power calculations
        for i, d in enumerate(adc_data[1:]):  # skip voltage ADC
            val = float(d)
            amps = val / ADC_RESOLUTION * ADC_VREF / SENSE_RESISTOR / GAIN
            if i > 0 and amps < 0:
                amps = 0  # remove ADC rounding errors at low currents
            watts = amps * volts
            json_data["amps"][device_list[i]] = round(amps, 2)
            json_data["watts"][device_list[i]] = round(watts, 1)
            total_load += watts
        json_data["volts"] = round(volts, 2)
        if total_load < 0:
            total_load = (
                0  # remove rounding errors so generator is always > 0
            )
        json_data["watts"]["generator"] = round(total_load, 1)
        for i, status in enumerate(device_status):
            if status in socket_status_codes:
                j = socket_status_codes[status]
            else:
                j = "ERROR"
            # device 0 = battery --> not used for now
            if i > 0:
                json_data["socket_settings"].append(j)
        json_data["shutdown_counter"] = int(time_data[0])
        json_data["shutdown_limit"] = int(time_data[1])
        # the numerator value comes from the microcontroller's lookup table
        # so it should never exceed the denominator, but just in case we'll
        # set the max value to 1
        json_data["battery_level"] = min(
            1, round(float(battery_level[0]) / float(battery_level[1]), 3)
        )
        for i, value in enumerate(duty_cycle):
            json_data["duty_cycle"][pwm_device_list[i]] = int(value)
        json_data["response"] = response.strip("\r\n")
        # logging.debug(json.dumps(json_data))
        return json_data
    except (ValueError, ZeroDivisionError):
        # couldn't parse string as float, usually because of incomplete data
        logging.error(
            f"value error: adc_data:{adc_data}, time_data:{time_data}, "
            f"device_status:{device_status}, battery_level:{battery_level}, "
            f" duty_cycle:{duty_cycle}, response:{response}"
        )
        return dict(error="incomplete data")
    except Exception as e:
        logging.exception("error converting to json")
        return dict(error="error converting to json")


def read_serial_port(conn):
    """
    reads the serial port and returns data in JSON format
    """
    import time  # for sleep() & time() instruction
    import urllib  # for url encoding and uploading data to db

    ############### start main program  ####################
    adc_data = []

    # test = serial.Serial(0)
    ##print("port is %s" % test.portstr)
    # dataline = serial.Serial(0)
    # dataline.open()
    #    #check if anything is in serial input buffer & empty it if so
    #    if dataline.inWaiting():
    #        ##print("flushing data...")
    #        x = dataline.read( dataline.inWaiting() )
    #        dataline.flushInput()
    try:
        # input data is raw ADC values in this order:
        # 'V, generator amps, [device amps], battery amps,
        #       elapsed time, seconds to shutdown'
        # this read has the data
        read_count = 0
        # try to read a valid data set three times before giving up
        # TODO: fix check for valid data
        while not adc_data and read_count < 3:
            # conn.write("R")
            # raw data format is: "Volts, ADC for {Battery, 1,2,3,4}; secs
            # elapsed, secs limit;on/off for each device|<debug_data>"
            try:
                raw_data = conn.readline().decode("ascii")
            except UnicodeDecodeError:
                logging.warning("error decoding unicode data")
                raw_data = None
            logging.debug(raw_data)
            if raw_data and parse_data(raw_data):
                (
                    adc_data,
                    time_data,
                    device_status,
                    duty_cycle,
                    battery_level,
                    response,
                ) = parse_data(raw_data)
                # fix problem when Vadc is four digits instead of three
                if len(adc_data[0]) > 3:
                    # invalid voltage reading --> need to re-read data
                    adc_data = []
                    read_count += 1
            else:
                read_count += 1
        # some reads may not retrieve a full data set, producing
        #   an "index out of range" error
        logging.debug(raw_data)
        # add line feed here to see if it solves problem with turning devices
        # on/off (
        # conn.write("\n")
        return convert_raw_to_dict(
            adc_data,
            time_data,
            device_status,
            duty_cycle,
            battery_level,
            response,
        )
    except serial.serialutil.SerialException:
        # device not found - wait and try again later
        logging.error("failed to make serial connection")
        return dict(error="failed to make serial connection")
        # return "Can't read power data from the PedalPC.  Is the USB cord plugged in?"
        return None
    except ValueError:
        # couldn't parse string as float, usually because of incomplete data
        logging.error("value error: %s" % raw_data)
        return dict(error="incomplete data")
        return None
    except:
        logging.exception("error in read_serial_port()")
        return dict(error="unknown error in read_serial_port()-check logs")
        return None


#    except:
#        # USB cord unplugged while in use
#        dataline.close()
#        return "USB cord plugged in?"

# this function is leftover from earlier and probably isn't needed
# unless you plan to write values to the microcontroller


def test():
    import glob
    import serial
    import platform
    import os

    logging.debug("platform = " + platform.system())
    # A function that tries to list serial ports on most common platforms
    system_name = platform.system()
    available = []
    if system_name == "Windows":
        # Scan for available ports.
        for i in range(256):
            try:
                s = serial.Serial(i)
                available.append(i)
                s.close()
            except serial.SerialException:
                pass
        return available
    elif system_name == "Darwin":
        # Mac
        return glob.glob("/dev/tty*") + glob.glob("/dev/cu*")
    else:
        # Assume Linux or something else
        # this does NOT return ttyACM0!
        # return glob.glob('/dev/ttyS*') + glob.glob('/dev/ttyUSB*')
        # this is supported in newer kernals
        if os.path.exists("/dev/serial"):
            available = glob.glob("/dev/serial/by-id/*")
        else:
            return glob.glob("/dev/ttyA*") + glob.glob("/dev/ttyUSB*")

    # connect using the first available port
    # print("realpath = %s " % os.path.realpath(available[0]))

    try:
        connection = serial.Serial(os.path.realpath(available[0]))
        # connection = serial.serial(available[0])
        logging.debug("opened real port %s" % connection.portstr)
        connection.close()
    except:
        logging.debug("could not open %s" % available[0])
    return


if __name__ == "__main__":
    conn = check_serial_port()
    print(f"serial connection: {conn}")
    if conn:
        seconds = 0
        watthrs = 0.0
        user_id = 3
        count = 1
        while count < 5:
            try:
                t = datetime.datetime.now()
                data = read_serial_port(conn)
                print(data)
                count += 1
            except Exception as e:
                print(e)
