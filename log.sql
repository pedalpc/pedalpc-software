CREATE TABLE log(
  id integer primary key,
  time timestamp,
  session text,
  user_id integer,
  volts numeric default 0.0,
  battery numeric default 0.0,
  socket_1 numeric default 0.0,
  socket_2 numeric default 0.0,
  socket_3 numeric default 0.0,
  socket_4 numeric default 0.0,
  socket_5 numeric default 0.0,
  socket_6 numeric default 0.0,
  fan numeric default 0.0,
  generator numeric default 0.0,
  foreign key(user_id) references user(id)
);
