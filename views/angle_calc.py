#! /usr/bin/python3
# requires python3.6 due to use of f-strings in last lines

from math import sin, cos, radians

# radius to center of gauge's semicircle
radius = 25.0
# thickness of the semicircle
band_thickness = 10
# how much the needle overlaps the semicircle on each end
needle_overlap = 2
# amount text hangs below semicircle
text_offset = 3
# x,y coordinates of the left-hand edge of the semicircle
origin = (needle_overlap + band_thickness / 2, needle_overlap + band_thickness / 2 + radius)
new_point = old_point = [] 
# green zone should be 1/2 or 2/3 of range
# each yellow zone should be 1/12 of range
# each red zone should be 1/12 or 1/6 of range
# therefore, total range should be twice green zone and centered on midpoint
gauge_name = 'volts'
min_green = 13.2     
#max_green = 13.5
max_green = 14.4
#gauge_name = 'battery-amps'
#min_green = 0      
#max_green = 3 
total_range = 2 * (max_green - min_green)
max_yellow = max_green + float(total_range) / 12
#max_red = max_yellow + float(total_range) / 6
max_red = max_yellow + float(total_range) / 12 
min_yellow = min_green - float(total_range) / 12
#min_red = min_yellow - float(total_range) / 6
min_red = min_yellow - float(total_range) / 12 
vals = [min_red, min_yellow, min_green, max_green, max_yellow, max_red]
colors = ['red', 'yellow', 'green', 'yellow', 'red']

new_point = origin
print("code for gauge:")
print('-'* 60)
print('<svg id="%s-gauge" class="battery-gauge" viewbox="0 0 %d %d">' % (
    gauge_name, 
    2 * (origin[0] + radius), 
    1 * (origin[1] + text_offset), 
))

for (i,x) in enumerate(vals[1:]):
    # calculate angle from horizontal of each segment using gauge values
    # (max value = 180 degrees), then convert to radians
    angle = radians(float(x - vals[0])/float(vals[-1] -  vals[0]) * 180)
    # use angle to calculate new ending X and Y
    old_point = new_point
    new_point = [
        origin[0] + radius * (1 -cos(angle)),
        origin[1] - radius * sin(angle)
    ]
    # draw arcs, beginning at end of last arc to end of new arc
    # (see https://www.sitepoint.com/closer-look-svg-path-data/ for arc syntax)
    print('    <path d="M %.2f,%.2f A%.0f,%.0f 0 0,1 %.2f,%.2f" style="stroke:%s; stroke-width:%s; fill:none;"></path>' % (
        old_point[0], old_point[1],
        radius, radius,
        new_point[0], new_point[1],
        colors[i],
        band_thickness
    ))
# draw needle
print('    <path id="%s-needle" d="M %d,%d v 2 h %d v -2 Z" transform="rotate(0, %d, %d)" style="stroke:#999; fill:white"/>' % (
    gauge_name, 
    0, 
    origin[1]-1, 
    2* needle_overlap + band_thickness,
    origin[0] + radius, 
    origin[1]
))
# placeholder for gauge value
print('    <text class="%s-text gauge-text" x="%d" y="%d">0</text>' % (
    gauge_name, origin[0] + radius, origin[1] + text_offset
))
print('</svg>')
print('-'* 60)
print('\ncode for updating needle position:')
print('-'* 60)
string = f'''
  document.getElementById("{gauge_name}-needle").setAttribute("transform",
    "rotate(" + 180.0 * (raw.{gauge_name}-{min_red:.1f})/({max_red:.1f}-{min_red:.1f}) + ",32,32)");
'''
print(string)
#
#    document.getElementById(
#        $(".%s-needle").attr(
#            "transform",
#            "rotate(" + 180.0 * (raw.%s-%.1f)/(%.1f-%.1f) + ",32,32)"
#        );
#'''[2:-1] % (       
#    gauge_name,
#    gauge_name,
#    min_red,
#    max_red,
#    min_red,
#))      
print('-'* 60)
print(vals)
print("min = %.1f, max = %.1f" % (min_red, max_red))




