# Launch server with:
#
#   /home/user1/venv/aiohttp/bin/python3 /home/user1/pedalpc_server/server.py
#
# Modifying conf files requires root privileges, so should be run as a system
#   process or using sudo
# This assumes username is user1 &  modules are installed in a virtual env
#   named 'aiohttp' under /home/user1/venv.  Change as necessary.

# change to working directory to read/write to database successfully
# (otherwise will get 'no table named user' error')
import os, sys
path = sys.path[0]
os.chdir(path)

# add to path to import modules
#sys.path.append('/home/jim/storage/Projects/venv/aiohttp/lib/python3.6/site-packages')

import aiohttp
from aiohttp import web, WSMsgType, WSCloseCode
import jinja2
import aiohttp_jinja2
import serial_asyncio       # from pyserial-asyncio module
import logging
import logging.handlers
import time
import datetime
import asyncio
import serial_asyncio
import json
import re
import serial_comm as sc
from modules import network as nm
import config
from modules import extras
import aiosqlite
import uuid

# logs to terminal by default (file logging controlled by crontab)
rh = logging.handlers.RotatingFileHandler(
    'pedalpc.log', maxBytes=100000, backupCount=2)
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s-%(module)s-%(lineno)d-%(levelname)s: %(message)s',
    datefmt="%Y-%m-%d %I:%M:%S %p %Z",
    handlers=[rh]
)

# dashboard settings
DEVICE_COUNT        = 9     # number of power sockets (incl. fan header)
GENERATOR_THRESHOLD = 3     # min. generator watts required to stored data etc

#TODO: replace or remove these values
#EMAIL           = 'jim@pedalpc.com'     # remove in production
SAMPLE_PERIOD   = 60            # seconds between each data save
SERVER_SHUTDOWN_PERIOD = 15     # seconds to allow server to shutdown b4 poweroff
DEBUG_MODE      = False         # set to False to log to production database etc

if DEBUG_MODE:
    SQLITE_DB = 'test.sqlite'
else:
    SQLITE_DB = 'pedalpc.sqlite'

app = web.Application()

# global variables
app['websockets'] = []
app['email'] = None         # get this from user cookie in production
#if DEBUG_MODE:
#    app['email'] = None
app['send_serial'] = None    # string to send out over serial port


# jinja2 templates are in 'views' subdirectory
aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('./views'))

# -------------------- utility functions ------------------------
async def get_power_history(mark_today=True):
    async with aiosqlite.connect(SQLITE_DB) as db:
        db.row_factory = aiosqlite.Row
        cursor = await db.execute('''
            SELECT power_data.date, CAST(minutes / 60 AS INT) AS hours,
                CAST(minutes % 60 AS INT) AS minutes,
                printf('%d', watthrs) AS watthrs,
                printf('%.1f', COALESCE(watthrs/minutes*60.0, 0)) AS average_watts
            FROM power_data
            JOIN user ON power_data.user_id = user.id
            WHERE user.email = ?
            ORDER BY power_data.date DESC
            LIMIT 7
            ''', (app['email'],))
        rows = await cursor.fetchall()
        data = []
        for r in rows:
            row = {}
            for k in r.keys():
                if mark_today and k == 'date' and r[k] == str(datetime.date.today()):
                    row[k] = 'Today'
                else:
                    row[k] = r[k]
            data.append(row)
        await cursor.close()
        return data

# -------------------- HTTP routing functions --------------------
@aiohttp_jinja2.template('login.jinja')
async def login(request):
    error_message = None
    if request.method == 'POST':
        form_data = await request.post()
        extra_days = 3  # keep them logged in over a weekend
        if form_data.get('remember_me'):
            extra_days = 30
        cookie_date = (datetime.datetime.now() + datetime.timedelta(
                days=extra_days)).strftime('%a, %d %b %Y %H:%M:%S')
        logging.debug('cookie_date=' + cookie_date)
        # TODO: figure out rules for changing users
#        if allow_user_change(request['REMOTE_ADDR']):
        logging.debug(form_data)
        if form_data.get('new_user') == '1':
            first_name = form_data.get('first_name')
            last_name = form_data.get('last_name')
            email = form_data.get('email')
            # check if passwords match
            if not re.match('[^@]+@[^@]+\.[^@]+', email):
                return {'error_message': 'invalid email address'}
            # check if email already exists in database
            q = 'SELECT * FROM user where email = ?'
            logging.debug(
                "adding (or updating) user '%s %s <%s>' in database" % (
                    first_name, last_name, email))
            async with aiosqlite.connect(SQLITE_DB) as db:
                # don't bother to check if the user is already in the db--they
                # they might want to just change the listed name
                # NOTE: sqlite's REPLACE INTO deletes a matching record then
                #   adds the updated records, thereby incrementing the id,
                #   so using this workaround instead.
                await db.execute('''INSERT OR IGNORE INTO user
                    (first_name, last_name, email) VALUES (?,?,?)''',
                    (first_name, last_name, email))
                await db.execute('''UPDATE user SET first_name = ?,
                    last_name = ? WHERE email = ?''',
                    (first_name, last_name, email))
                # TODO: check if internet connection present first!
                # TODO: send welcome email if internet connection present
                # TODO: add user to remote db (need to write script for this
                #       first!)
#                url = 'https://www.pedalpc.com/user/insert'
#                await update_remote_db(url, data)
                await db.commit()
                logging.debug('finished adding or updating user in db')
        else:
            email = form_data.get('email')
            if not email:
                # need to fill in at least the email address
                logging.debug('email address missing')
                return {'error_message': "Email address is required"}
            async with aiosqlite.connect(SQLITE_DB) as db:
                db.row_factory = aiosqlite.Row
                cursor = await db.execute('''
                    SELECT * from user where email = ?''', (email,))
                row = await cursor.fetchone()
                await cursor.close()
                if not row:
                    error_message = "'%s' is not in the database" % email
                    logging.debug(error_message)
                    return {'error_message': error_message}
        # first user is the one who's name is shown
        if not app['email']:
            app['email'] = email
        response = web.HTTPFound('./dashboard')
        logging.debug("setting user cookie to '%s', expiring %s" % (
                email, cookie_date))
        response.set_cookie('email', email, expires=cookie_date)
        logging.debug(f"response.cookie is {response.cookies}")
        # raising the response prevents the cookie from being set in V3.7, see:
        #   https://github.com/aio-libs/aiohttp/issues/5181
        #raise response
        return response
    return {'error_message': error_message}

async def dashboard(request):
    logging.debug(request)
    email = request.cookies.get('email')
    logging.debug("user email from cookie is %s" % email)
    if not email:
        # new user or just missing cookie?
        logging.debug("missing email, so redirecting to login")
        raise web.HTTPFound('./login')
    #TODO: return dashboard as "read-only", without user info
    #  or ability to login
    #TODO: handle how to logout and switch to new user
    # (this may not be the best way to handle this!)
    if not app['email']:
        # this person is now the current user
        app['email'] = email
    else:
        # this person is watching someone else use the machine
        pass
    # create default list of labels (fan label is fixed and doesn't change)
    socket_labels = ['socket #%s' % (i) for i in range(1, DEVICE_COUNT)]
    async with aiosqlite.connect(SQLITE_DB) as db:
        # get first_name, list of connected devices, past wifi settings,
        # and past user history
        db.row_factory = aiosqlite.Row
        cursor = await db.execute('''
            SELECT first_name from user where email = ?''', (email,))
        d = await cursor.fetchone()
        if d:
            first_name = d['first_name']
        else:
            logging.debug("user cookie exists, but %s not in database!" % email)
            # odd--cookie exists but email is not in database....
            raise web.HTTPFound('./login')
        # pull in any that were created earlier
        cursor = await db.execute('''
            SELECT id, label FROM device ORDER BY id''')
        device_list = await cursor.fetchall()
        # merge the two together
        for d in device_list:
            socket_labels[d['id']-1] = d['label']
    history = await get_power_history()
    # create blank record for today in history if needed
    today = dict(date = 'Today', hours = 0,
        minutes = 0, watthrs = 0, average_watts = 0)
    if not history:
        # new user!
        history = [today]
    elif history[0]['date'] != 'Today':
        # no data yet for today (start of new day)
        history.insert(0, today)
    # prevent the page from being cached so latest history totals are pulled
    # from the server instead of using previous starting values
    headers = {
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        'Expires': '0'
    }
    # TODO: get status of devices to render buttons properly on first render
    # set template values
    context = {
        'first_name': first_name,
        'user': email,
        'socket_labels': socket_labels,
        'history': history[:7],
        }
    response = aiohttp_jinja2.render_template('dashboard.jinja', request,
            context)
    # prevent the page from being cached so latest history totals are pulled
    # from the server instead of using previous starting values
#    response.headers = {
#        'Cache-Control': 'no-cache, no-store, must-revalidate',
#        'Expires': '0'
#    }
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    response.headers['Expires'] = '0'
    # set user's login cookie to expire 1 week from now...
    expiration = datetime.datetime.now() + datetime.timedelta(days=7)
    if expiration.year > 2017:
        # ..but if the computer has restarted, only update cookie if
        # computer has re-synced it's clock to the # correct time
        logging.debug("setting user cookie to %s, expires %s" %
            (email, expiration))
        response.set_cookie('user', app['email'], expires=expiration)
    else:
        logging.debug("not updating cookie--clock hasn't updated after reboot so expiration date is wrong (%s)" % expiration)
    return response

async def update_ap_list(request):
    '''
    update the list of available access points
    '''
    # result is a list of all APs, with the active one set to True, all
    # others set to False
    aps = nm.get_wifi_access_points()
    if not aps:
        return web.json_response({'html':'<option><em>Unable to scan for wifi networks</em></option>'})
    html = ''
    for k in aps:
        if aps[k]:
            html += '<option selected=selected>%s</option>\n' % k
        else:
            html += '<option>%s</option>\n' % k
    return web.json_response({'html':html})

@aiohttp_jinja2.template('settings.jinja')
async def settings(request):
    '''
    get or update the PedalPC's network and hotspot settings
    best to redraw entire settings form since settings are interdependent
    (e.g., adding a wired connection makes the wifi connection editable)
    '''
    messages = []
    message_type = None
    if request.method == 'POST':
        data = await request.post()
        logging.debug({f"{k}: {data[k]}" for k in data})
        # these are the only two fields that will be definitely received
        # All others can be disabled, so they may or may not be sent
        is_wifi_locked = str(data['wifi_locked']).strip()
        hotspot_onoff = str(data['hotspot_onoff']).strip()
        # change wifi connection if necessary
        if is_wifi_locked == 'false':
            wifi_ssid = str(data['wifi_ssid']).strip()
            wifi_password = str(data['wifi_password']).strip()
            # don't change unless changing to a new AP
            if wifi_ssid != nm.get_current_ssid():
                error_msg = nm.connect_wifi(wifi_ssid, wifi_password)
                if error_msg:
                    messages.append(error_msg)
        # change hotspot if necessary
        if hotspot_onoff == 'on':
            logging.debug('hotspot on')
            # these are only passed if hotspot is set to 'on'
            hotspot_ssid = str(data['hotspot_ssid']).strip()
            hotspot_password = str(data['hotspot_password']).strip()
            hotspot_ipv4_3 = str(data['hotspot_ipv4_3']).strip()
            if not isinstance(int(hotspot_ipv4_3), int) or not (
                    0 < int(hotspot_ipv4_3) < 255):
                messages.append(
                    'hotspot address box must be between 0 and 255')
            else:
                #TODO: update AP's IPv4 address in /etc/dnsmasq.conf
                #   and /etc/dhcpcd.conf
                nm.update_ap_ip4(hotspot_ipv4_3)
                error_msg = nm.start_ap(hotspot_ssid, hotspot_password)
                if error_msg:
                    messages.append(error_msg)
        else:
            logging.debug('hotspot off')
            nm.stop_ap(nm.ap_iface)
        if len(messages) > 0:
            message_type = 'error'
        else:
            messages.append('Settings successfully updated')
            message_type = 'success'
        # Updating AP requires temporarily dropping the wifi connection, so we'll
        # wait a bit for wifi connection to re-establish. Otherwise it often returns
        # 'disconnected'.
        time.sleep(3)
    # get status of all current connections
    active_connections = nm.get_connections(active=True)
    is_wired_up = True if nm.wired_iface in active_connections else False
    is_wifi_up = True if nm.wifi_iface in active_connections else False
    is_hotspot_up = True if nm.ap_iface in active_connections else False
    # don't allow wifi settings to be changed if it's the sole
    # connection, as that will cause it to disconnect
    is_wifi_locked = True
    if is_wired_up or nm.is_hotspot_in_use(request.remote):
        is_wifi_locked = False
    # don't allow hotspot to be modified if not connected to LAN,
    # otherwise it will disconnect
    is_hotspot_locked = True
    if (is_wired_up or is_wifi_up) and not nm.is_hotspot_in_use(request.remote):
        is_hotspot_locked = False
    # get hotspot attributes
    hotspot_ssid = nm.get_current_ssid(nm.ap_iface)
    if hotspot_ssid:
        hotspot_password = nm.get_ap_password()
    hotspot_ipv4_octets = nm.get_ip4_octets(nm.ap_iface)
    # get dict of available wifi APs in the area
    ap_dict = nm.get_wifi_access_points()
    return locals()


@aiohttp_jinja2.template('feedback.jinja')
async def feedback(request):
    messages = []
    message_type = None
    if request.method == 'POST':
        # assume bad input by default
        message_type = 'error'
        data = await request.post()
        logging.debug({f"{k}: {data[k]}" for k in data})
        name = str(data['name']).strip()
        email = str(data['email']).strip()
        message = str(data['message']).strip()
        if not name:
            messages.append('please enter your name')
        if not re.match('\S+@\S+.\S+', email):
            messages.append('email address is not valid')
        if not message:
            messages.append("you didn't enter feedback or comment")
        # only process input if there are no error messages
        if len(messages) == 0:
            async with aiosqlite.connect(SQLITE_DB) as db:
                cursor = await db.execute('''
                    INSERT INTO feedback (name, email, message)
                    VALUES (?,?,?)
                    ''', (name, email, message))
                await db.commit()
            name = ''
            email = ''
            message = ''
            messages.append("Thanks for your feedback!  We will respond shortly")
            message_type = 'success'
    return locals()

@aiohttp_jinja2.template('wired_status.jinja')
async def wired_connection(request):
    '''
    get current state of wired connection
    '''
    # check if connected to LAN
    is_up = False
    if nm.wired_iface in nm.get_connections(active=True):
        is_up = True
    return locals()

@aiohttp_jinja2.template('wireless_status.jinja')
async def wireless_connection(request):
    '''
    get - return current state of wireless connection
    post - connect to AP
    '''
    # check if connected to LAN
    is_up = False
    if nm.wifi_iface in nm.get_connections(active=True):
        is_up = True
    # check if other connection available to see if the connection is modifiable
    cannot_modify = True
    if (nm.wired_iface in nm.get_connections(active=True) or
            nm.is_hotspot_in_use(request.remote)):
        cannot_modify = False
    # get dict of available wifi APs in the area
    ap_dict = nm.get_wifi_access_points()
    return locals()

@aiohttp_jinja2.template('hotspot.jinja')
async def hotspot(request):
    '''
    get - return current state of hotspot
    post - turn hotspot on or off and/or update it's SSID and password
    '''
    error_msg = None
    if request.method == 'POST':
        data = await request.post()
        logging.debug(data)
        # parse everything as a string just in case...
        ap_on = str(data['ap_on']).strip()
        ap_name = str(data['ap_name']).strip()
        ap_password = str(data['ap_password']).strip()
        if ap_on == 'yes':
            logging.debug('hotspot should be on')
            error_msg = nm.ap_start(ap_name, ap_password)
        else:
            logging.debug('hotspot off')
            nm.ap_stop()
    ap_on = True if nm.ap_iface in nm.get_connections(active=True) else False
    ap_name = nm.get_current_ssid(nm.ap_iface)
    ap_password = nm.get_ap_password()
    ap_ipv4_octets = nm.get_ip_octets(nm.wifi_iface)
    return locals()

async def history(request):
    return web.json_response(await get_power_history())

# -------------------- websocket handling functions --------------------
async def websocket_handler(request):
    '''
    handle incoming websocket requests
    '''
    resp = web.WebSocketResponse()
    await resp.prepare(request)
    request.app['websockets'].append(resp)

    try:
        async for msg in resp:
            logging.debug(msg)
            if msg.type == WSMsgType.TEXT:
                logging.debug('got ws request: "%s"' % msg.data)
                # save serial string to send (if necessary), based on
                # what client requested
                app['send_serial'] = await parse_client_command(msg.data)
                # TODO: process error if response == 'ERROR'
            elif msg.type == WSMsgType.ERROR:
                logging.error('ws connection closed with exception %s' %
                      ws.exception())
    finally:
        request.app['websockets'].remove(resp)
        logging.debug('lost a websocket client')
    return resp

async def parse_client_command(client_str):
    '''
    parse a client's websocket command and perform an action based on command
    '''
    # convert string from client to dictionary
    d = json.loads(client_str)
    # this will be the command, if any, to send back over the serial port
    # based on the client command
    serial_command = None
    logging.debug(f"got {d} in parse_client_command")
    # "type" determines the action to take
    if "type" in d:
        if d["type"] == 'change_socket':
            # change a socket setting
            serial_command = sc.change_device(d['device_num'],d['new_state'])
        elif d["type"] == 'change_duty_cycle':
            # change a socket's duty cycle
            serial_command = sc.change_duty_cycle(d['device_num'],d['value'])
        elif d["type"] == 'set_timeout':
            # change the timeout period before the battery shuts off
            serial_command = sc.set_timeout(d['amount'])
        elif d["type"] == 'update_label':
            # update the socket label in the database
            await update_label(d['socket_number'],d['label'])
    logging.debug(f"serial command is {serial_command}")
    return serial_command

async def update_label(socket_number, label):
    '''
    update a label associated with a socket in the database
    '''
    logging.info('updating socket %s to "%s"' % (socket_number, label))
    if len(label) > 30:
        label = label[:30]
    # socket_number is parsed as an integer!
    if ((socket_number < DEVICE_COUNT) and
            re.match('^[\w\s+&/@#-]{2,30}$', label)):
        async with aiosqlite.connect(SQLITE_DB) as db:
            logging.info('updating db socket %s to "%s"' % (
                    type(socket_number), label))
            # REPLACE shorthand method for INSERT OR REPLACE (in sqlite)
            cursor = await db.execute('''REPLACE INTO device (id, label)
                VALUES (?,?)''', (socket_number, label))
            await db.commit()
    else:
        #TODO: handle error here by passing error message back as new label
        # or something else(?)
        logging.warning('could not update socket label')
    return


# ------------------- background tasks ---------------------
async def read_serial_port(app):
    '''
    open serial port, read data from it, and send data to clients
    '''
    reader, writer = await serial_asyncio.open_serial_connection(
        url=sc.SERIAL_PORT, baudrate=sc.SERIAL_BPS)
    # send "go" command to start receiving data
    writer.write("G\r\n".encode('ascii'))
    logging.debug("sent command G\r\n")
    resp = await reader.readline()
    try:
        # Should be "ACK" if data stream hasn't begun yet
        logging.debug("got response:" + resp.decode('ascii'))
    except UnicodeDecodeError:
        # Occurs when already streaming data and stream is mangled.
        pass
    except Exception as e:
        logging.exception(e.msg)
    # Used for determining averages
    sample_counter = 0
    # User might not pedal throughout entire sample period,
    # so need to include a counter to accurately calculate
    # average power output during sample period
    pedaling_counter = 0
    avg_data = {}
    end_time = time.time() + SAMPLE_PERIOD  # take averages & store data then
    while True:
        try:
            resp = await reader.readline()
            if not resp:
                break
            # parse data into an array
            try:
                logging.debug(resp)
                raw_data = sc.parse_data(resp.decode('ascii'))
                logging.debug(raw_data)
            except:
                # fix for rare random UnicodeDecode error
                raw_data = None
            if raw_data:
                # convert array into useful values and store in dict
                data = sc.convert_raw_to_dict(*raw_data)
            else:
                data = None
            # TODO: log data here
            logging.debug("%s, user:%s, counter:%s, # ws:%d" % (
                    data, app['email'], sample_counter, len(app['websockets'])))
            if data:
                sample_counter += 1
                if data['watts']['generator'] > GENERATOR_THRESHOLD:
                    pedaling_counter += 1
                if ((data['shutdown_limit'] - data['shutdown_counter']) <=
                        SERVER_SHUTDOWN_PERIOD):
                    os.system('shutdown now')
                if bool(avg_data):
                    for k in data['watts']:
                        avg_data[k] += data['watts'][k]
                    avg_data['volts'] += data['volts']
                else:
                    for k in data['watts']:
                        avg_data[k] = data['watts'][k]
                    avg_data['volts'] = data['volts']
            else:
                logging.warning("no data; counter: %d" % sample_counter)
            if time.time() >= end_time:
                # advance the stop timer 1 min
                end_time = time.time() + SAMPLE_PERIOD
                for k in avg_data:
                    if k != 'email':
                        avg_data[k] = avg_data[k]/sample_counter
                if app['email']:
                    avg_data['email'] = app['email']
                logging.debug("avg_data: %s" % avg_data)
                if avg_data:
                    await update_local_db(
                        avg_data, 
                        pedaling_counter / sample_counter, 
                    )
                if data and config.ENABLE_BATTERY_SVG == True:
                    # use latest data point here since battery_level
                    # isn't a key in avg_data
                    await extras.create_battery_svg(data)
                #if DEBUG_MODE:
                #    url = 'https://www.pedalpc.com/log/test'
                #else:
                #    url = 'https://www.pedalpc.com/log'
                #await update_remote_db(url, avg_data)
                sample_counter = 0
                pedaling_counter = 0
                avg_data = {}
            # send data to clients
            for ws in app['websockets']:
                #logging.debug('sending to client %s' % json.dumps(data))
                try:
                    await ws.send_str(json.dumps(data))
                except:
                    logging.exception('error sending data to client')
            if app['send_serial']:
                logging.info("sending '%s' to serial port" % app['send_serial'])
                # send each character individually with a small wait period
                # between them to reduce misreads
                for x in app['send_serial']:
                    writer.write(x.encode('ascii'))
                    logging.info("wrote '%s' to serial port" % x.encode('ascii'))
                    time.sleep(0.2)
                #writer.write(app['send_serial'])
                app['send_serial'] = None
        except asyncio.CancelledError:
            # this is necessary to stop the server; otherwise, it will hang
            logging.info('server stopped')
            writer.close()

#        # need to check if any connections have been established yet, otherwise
#        # the queue fills up and throws an error
#        for i,ws in enumerate(app.sockets):
##                print('socket #{}: connection_lost:{}'.format(i,ws._connection_lost))
##                pprint(vars(ws), indent=2)
##                print('-' * 60)
##            if len(app.sockets):
#            logging.debug(len(app.sockets))
#            app.queue.put_nowait(data)
#            try:
#                data_str = app.queue2.get_nowait()
#                if data_str:
#                    # parse and do what the client requested
#                    command = await parse_client_command(data_str)
#                    # parsing will produce a command to send to the
#                    # micorocontroller when necessary
#                    if command:
#                        logging.debug("sending '%s' to serial port" % command)
#                        writer.write(command)
#            except asyncio.queues.QueueEmpty:
#                pass

async def update_local_db(data, generated_fraction):
    """
    Stores minute-by-minute data in the table 'log', and adds user's energy
    output to daily total in the table 'power_data'.
    `generated_fraction` is the fraction of the period the user was pedaling 
    and generating electricity
    """
    # sqlite uses UTC for date/times, but dashboard shows everything
    # in local time.  We'll use local time in our database tables
    # to be consistent, which requires correcting for daylight savings time
    logging.debug(data)
    try:
        async with aiosqlite.connect(SQLITE_DB) as db:
            db.row_factory = aiosqlite.Row
            # Each period when someone is pedaling is assigned a
            # unique session ID, so power output can be tracked before the
            # user's email address is known.  If no one is pedaling, the
            # data is still logged, but no session number is assigned.
            if data['generator'] > GENERATOR_THRESHOLD:
                # generator output exceeds threshold, so retrieve previous
                # session ID.  If none exists, create one
                cursor = await db.execute(
                    'SELECT session FROM log ORDER BY id DESC LIMIT 1'
                )
                row = await cursor.fetchone()
                await cursor.close()
                session_id = row[0] if (row and row[0]) else str(uuid.uuid4())
            else:
                # no one is pedaling, so there is no session ID
                session_id = None
            # --- append data to log ---
            logging.info(
                f"appending avg_data {data} to log "
                f"with session = {session_id} "
                f"and pedaling fraction = {generated_fraction}"
            )
            await db.execute('''
                INSERT INTO log (
                    session, time, volts, battery, socket_1, 
                    socket_2, socket_3, socket_4, socket_5, socket_6, 
                    socket_7, socket_8, fan, generator, generated_fraction
                ) VALUES (
                    ?, datetime('now', 'localtime'), ?, ?, ?, 
                    ?, ?, ?, ?, ?, 
                    ?, ?, ?, ?, ?
                )''' , (session_id, data['volts'], data['battery'],
                    data['socket_1'], data['socket_2'], data['socket_3'],
                    data['socket_4'], data['socket_5'], data['socket_6'],
                    data['socket_7'], data['socket_8'], data['fan'],
                    data['generator'], generated_fraction)
                )
            # delete any data from log prior to last 24 hours, to prevent
            #   log from growing too large
            logging.debug("removing old log data from log table")
            await db.execute('''
                DELETE FROM log WHERE ID in (
                    SELECT id FROM log ORDER BY id DESC
                    LIMIT 24*60, 10
                )
             ''')
            # --- now add to this user's power_data ---
            if 'email' in data and data['email'] and data['generator'] > GENERATOR_THRESHOLD:
                # get energy already produced by user already today
                cursor = await db.execute('''SELECT minutes, watthrs
                    FROM power_data
                    JOIN user ON power_data.user_id = user.id
                    WHERE user.email = ?
                    AND power_data.date = date('now','localtime')''',
                    (data['email'],))
                d = await cursor.fetchone()
                await cursor.close()
                logging.debug('user query result: %s', d)
                if not d:
                    # No data yet for this user, so all energy generated
                    # so far during this session was created by this user.
                    # We'll save the unattributed data first for this user,
                    # then add the latest data to it after.
                    #
                    # Need to use COALESCE here, because the SUM() function
                    # returns no values if there are no matches.
                    cursor = await db.execute('''
                        SELECT COALESCE(sum(generated_fraction), 0.0) AS minutes,
                            COALESCE(SUM(generator), 0.0) 
                                AS watt_minutes
                            FROM log WHERE session = ?
                        ''', (session_id, ))
                    row = await cursor.fetchone()
                    await cursor.close()
                    minutes = watt_minutes = 0
                    if row:
                        (minutes, watt_minutes) = row
                        logging.info(
                            f"extracted {watt_minutes} watt-minutes "
                            f"and {minutes} minutes from log"
                        )
                    # convert total "watt-minutes" to watt-hours
                    watthrs = watt_minutes / 60.0
                    await db.execute('''
                        INSERT INTO power_data (
                            date, minutes, watthrs, user_id
                        ) VALUES (
                            date('now','localtime'), ?, ?, (
                                SELECT id FROM user where email = ?)
                        )''' , (minutes, watthrs, data['email']))
                    logging.info(
                        f"using log to add {watthrs} whrs and "
                        f"{minutes} minutes to power_data table "
                        f"for {data['email']}"
                    )
                else:
                    # Average watts over one minute is 1 watt-minute,
                    # so divide by 60 to convert to watt-hours
                    watthrs = data['generator'] / 60
                    # User may not have stopped or started pedaling during
                    # sample period, so include only portion of
                    # minute user was pedaling.
                    await db.execute('''UPDATE power_data set
                            minutes = minutes + ?, watthrs = watthrs + ?
                        WHERE date = date('now', 'localtime') AND user_id = (
                            SELECT id FROM user WHERE email = ?
                        )''',  (generated_fraction, watthrs, data['email']))
                    logging.info(
                        f"added {watthrs} whrs and {generated_fraction} min"
                        f" to power_data table for user {data['email']}"
                    )
            await db.commit()
            # this throws a ValueError("no active connection")
            #await db.close()
            logging.debug('successfully updated local database')
    except:
        # Usually a key error due to bad data.  Log the problem
        # and go on
        logging.exception('could not update local db')
        pass
    return

async def update_remote_db(url, data):
    """
    upload data to remote server
    """
    logging.debug("posting %s to %s using app['session']" % (data, url))
    async with app['session'].post(url, data=data) as resp:
            logging.debug( 'Data posted.  Response code: %s' % resp.status)
#        # use simplest type of request and force-close connection when done
#        # to try and prevent memory leaks, but this didn't provide any
#        # improvement--app uses 202 MB when running regardless
#        connector = aiohttp.TCPConnector(enable_cleanup_closed=True,
#                force_close=True)
#        async with aiohttp.request('POST',url, data=data,
#                connector=connector) as resp:
#        async with aiohttp.request('POST',url, data=data,
#                timeout=timeout) as resp:
#



# ------------------- start tasks, routing, and server ---------------------
async def start_background_tasks(app):
    logging.debug('Starting serial background task...')
    # create single session for posting data to remote server per aiohttp FAQ:
    # https://docs.aiohttp.org/en/stable/faq.html#how-do-i-manage-a-clientsession-within-a-web-server
    # set timeout to post to remote server to 10 s (default time out is 5 min!)
    timeout = aiohttp.ClientTimeout(total=10)
    app['session'] = aiohttp.ClientSession(timeout=timeout)
    app['serial'] = app.loop.create_task(read_serial_port(app))

async def on_shutdown(app):
    logging.info('shutting down in on_shutdown')
    for ws in set(app['websockets']):
        await ws.close(code=WSCloseCode.GOING_AWAY, message='Server shutdown')

async def cleanup_background_tasks(app):
    logging.info('cleaning up background tasks')
    app['serial'].cancel()
    await app['serial']


# routing can also be done the "Flask way" via route decorators, e.g.
#   routes = web.RouteTableDef()
#
#   @routes.get('/index')
#   async def handle_index(request):
#       ....
# routing the "Django way" using a routing table
app.router.add_routes([
    web.route('*', '/login', login),
    web.get('/dashboard', dashboard),
    web.get('/', dashboard),
    web.get('', dashboard),
    web.get('/update', websocket_handler),
    web.get('/history', history),
    web.get('/update-ap-list', update_ap_list),
    web.route('*','/wired', wired_connection),
    web.route('*','/wireless', wireless_connection),
    web.route('*','/hotspot', hotspot),
    web.route('*','/feedback', feedback),
    web.route('*','/settings', settings),
    web.static('/static', './static')
    # process the rest using web sockets
    #web.get('/feedback', feedback),
])

if __name__ == '__main__':
    # launch serial port backgound task
    app.on_startup.append(start_background_tasks)
    app.on_shutdown.append(on_shutdown)
    app.on_cleanup.append(cleanup_background_tasks)
    # default host = '0.0.0.0'
    web.run_app(app, port=8081)
