$(document).ready(function() {
  localStorage.numWins = 0;
  localStorage.numLost = 0;
	var hangman = {
		alphabetArray: ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"],
		wordList: [],
		assetsLoaded: 0,
		theWord: "",
		guessWord: [],
		newGuessWord: "",
		theCanvas: $("#hangmancanvas").get(0),
		numWrong:0,
		gameOver:false,
		imageCounter: 0,
	    imageInterval: null,
		hangmanSheet: new Image(),
		//danceMusic: new Audio(),
		guessedLetters:[]
	}
	var ctx = hangman.theCanvas.getContext("2d");
	//var danceMusicAudio = "danceMusic"+getAudioExtension(hangman.danceMusic);

	hangman.hangmanSheet.addEventListener("load",incrementAssetsLoaded,false);
	hangman.hangmanSheet.src = "/static/images/hangman_sprite_sheet.png";
	
	//hangman.danceMusic.src = danceMusicAudio;
	//hangman.danceMusic.addEventListener('canplaythrough',incrementAssetsLoaded,false);
	//hangman.danceMusic.load();

	$.ajax({
		type: 'GET',
		url: "/static/js/hangman_wordlist.txt",
		success: function( data ) {
			hangman.wordList = data.split("\r\n");
			if (hangman.wordList.length === 1) {  //no \r\n so assume \n alone is linebreak
				hangman.wordList = data.split("\n");
			}
			incrementAssetsLoaded();
		},
		dataType: "text"
    });
	
	function incrementAssetsLoaded(){
	
	    hangman.assetsLoaded+=1;
		if(hangman.assetsLoaded == 2){
		startGame();
		}
	}
	
	function startGame(){
		createGuessWord();
		drawCanvas();
		enableButtons();
		addKeyListener();
   }
   
   function doWin(){
		hangman.imageCounter = 0;
		//hangman.danceMusic.currentTime=0;
		updateWinScore();
		hangman.imageInterval = setInterval(drawWinScreen,20);
	}
	
	function drawWinScreen(){
	clearCanvas(ctx,hangman.theCanvas);
	if(hangman.imageCounter >= 85){
		clearInterval(hangman.imageInterval);
    if (window.confirm("Click OK to play again, Cancel to quit")) {
      setTimeout(clearAndRestartGame,1000);
    } else {
      $('.game-wrapper').hide(); 
      $('.survey-form').show();
    }
	}
	ctx.drawImage(hangman.hangmanSheet,164*hangman.imageCounter,0,164,264,136,58,164,264);
    drawText("YOU WIN!!",50,375,"bold 50px sans-serif","#0000FF");
    hangman.imageCounter++;
	//hangman.danceMusic.play();
   }
   
   function clearAndRestartGame(){
		//hangman.danceMusic.pause();
		doGameOver();
	}
	
	function doGameOver(){
	hangman.numWrong = 0;
	hangman.gameOver = false;
	hangman.guessedLetters = new Array();
    startGame();
	}
   function createGuessWord(){
		hangman.guessWord = new Array();
		var randomWord = Math.floor(Math.random() * hangman.wordList.length);
		hangman.theWord = hangman.wordList[randomWord];
		if(hangman.theWord.length < 3 || hangman.theWord.length > 12){
			createGuessWord();
		}

	
		for(var i = 0; i< hangman.theWord.length;i++){
			if(hangman.theWord.charAt(i) == "-"){
				hangman.guessWord[i] ="-";
			}else{
			hangman.guessWord[i]="_";
			}
		}
		hangman.newGuessWord = hangman.guessWord.join(" ");
	}
	
	function clearCanvas(context, canvas) {
		context.clearRect(0, 0, canvas.width, canvas.height);
		var w = canvas.width;
		canvas.width = 1;
		canvas.width = w;
	}
	
	function drawGallows(){
		ctx.moveTo(120,305);
		ctx.lineTo(280,305);
		ctx.moveTo(260,305);
		ctx.lineTo(260,70);
		ctx.lineTo(180,70);
		ctx.lineTo(180,96);
		ctx.stroke();
	
	}
	function drawHead(){
	    ctx.beginPath();
		ctx.arc(180,120,23,0,Math.PI*2,false);
		ctx.closePath();
		ctx.stroke();
    }
	function drawBody(){
	      ctx.moveTo(180,143);
		  ctx.lineTo(180,248);
		  ctx.stroke();
   }
   function drawArm1(){
	ctx.moveTo(180,175);
	ctx.lineTo(142,167);
	ctx.stroke();
	}
	function drawArm2(){
	  ctx.moveTo(180,175);
      ctx.lineTo(218,167);
      ctx.stroke();
   }
    function drawLeg1(){
	  ctx.moveTo(180,245);
	  ctx.lineTo(145,270);
	  ctx.stroke();
  }
   function drawLeg2(){
	  ctx.moveTo(180,245);
	  ctx.lineTo(215,270);
	  ctx.stroke();
  }
   
	function drawHangman(drawNum){
		  switch(drawNum)
			{
			 case 0:
				drawGallows();
			 break;
			 case 1:
			 	drawHead();
			 break;
			 case 2:
				drawBody();
			 break;
			 case 3:
				drawArm1();
			 break;
			 case 4:
			 	drawArm2();
			 break;
			 case 5:
			 	drawLeg1();
			 break;
			 case 6:
			 	drawLeg2();
			 break;
		}
	  }
	function drawText(word,textX,textY,font,color){
        ctx.font = font;
		ctx.fillStyle = color
		ctx.fillText(word, textX, textY);
    }
	
    function drawCanvas(){
	  clearCanvas(ctx,hangman.theCanvas);
    
		for(var i=0;i<=hangman.numWrong;i++){
			drawHangman(i);
		}
	 
	  if(hangman.gameOver){
      disableButtons();
      removeKeyListener();
      drawText("Sorry, you lost",3300,200, "bold 40px sans-serif","#FF0000");
      drawText(hangman.theWord,50,27,"bold 35px sans-serif","#0000FF");
      updateLostScore();
      if (window.confirm("Click OK to play again, Cancel to quit")) {
        setTimeout(doGameOver,1500);
      } else {
        $('.game-wrapper').hide(); 
        $('.survey-form').show();
      }
      
		}else{
		drawText(hangman.newGuessWord,50,27,"bold 35px sans-serif","#0000FF");
	}
	  drawText("Games Won "+getWinScore(),120,350,"bold 20px sans-serif","#0000FF");
	  drawText("Games Lost "+getLostScore(),120,380,"bold 20px sans-serif","#FF0000");
	}
	
	for(var i = 0; i < hangman.alphabetArray.length; i++) {
		$('<button/>', {
		   text: hangman.alphabetArray[i], 
		   id: 'btn_'+hangman.alphabetArray[i],
		   width: "30px",
		   click: function (event) {
		   checkGuess(event,false);
		   }
		}).appendTo("#buttondiv");
	}
    function disableButtons(){
		$("#buttondiv button").attr("disabled","disabled");
	}
	disableButtons();
	
	function enableButtons(){
		$("#buttondiv button").removeAttr("disabled");
	}
	
	function addKeyListener(){
		$(document).on("keyup",function(event){
			checkGuess(event,true);
	
		});
	}
	
	function removeKeyListener(){
		$(document).off("keyup");
	}
	function checkGuess(event,isKeyPress){
		var currentButton;
		var theLetter;
		var RegEx = /[a-zA-Z]/;
	    var correctGuess = false;

    if(isKeyPress){
		currentButton = "btn_"+String.fromCharCode(event.keyCode);
		theLetter = $("#"+currentButton).text().toLowerCase();
		$("#"+currentButton).attr("disabled", "disabled");
		if(!RegEx.test(theLetter)){
			return;
		}
	}else{
	   currentButton = $(event.target);
	   $(currentButton).attr("disabled", "disabled");
	   theLetter = $(currentButton).text().toLowerCase();
	}
	
	if(hangman.guessedLetters.indexOf(theLetter) >=0){
		return;
	}else{
		hangman.guessedLetters.push(theLetter);
	}
		
		for(var i =0;i<hangman.theWord.length;i++){
			if(hangman.theWord.charAt(i) == theLetter){
				hangman.guessWord[i] = theLetter;
				correctGuess = true;
			}
		}
			hangman.newGuessWord = hangman.guessWord.join(" ");
			
		if(!correctGuess){
			hangman.numWrong++
		}
		if(hangman.newGuessWord.replace(/ /g,'') == hangman.theWord){
		  disableButtons();
		  removeKeyListener();
		  setTimeout(doWin,1500);
		}
		if(hangman.numWrong == 6){
			hangman.gameOver = true;
		}
		drawCanvas();
    }
	
	function getAudioExtension(audio) {
		var extension = "";
		if (audio.canPlayType("audio/ogg") =="probably" || audio.canPlayType("audio/ogg") == "maybe") {
			
			extension = ".ogg";
		}else if(audio.canPlayType("audio/mp3") == "probably" || audio.canPlayType("audio/mp3") == "maybe") {
			
			extension = ".mp3";
		}
		return extension;
	}
	
	function setWinScore(){
		if(!localStorage.numWins){
			localStorage.numWins = 0;
		}
	}
	setWinScore();
	function updateWinScore(){
		localStorage.numWins = Number(localStorage.numWins)+1;
	}
	
	function getWinScore(){
		if(localStorage.numWins){
			return localStorage.numWins;
		}
	}
	function setLostScore(){
		if(!localStorage.numLost){
			localStorage.numLost = 0;
		}
	}
	setLostScore();
	
	function updateLostScore(){
		localStorage.numLost = Number(localStorage.numLost)+1;
	}
	
	function getLostScore(){
		if(localStorage.numLost){
			return localStorage.numLost;
		}
	}

  function askNewGame() {
    $(".game-wrapper").hide();
    $(".dialog-wrapper").show();
  }
});
