#! /bin/bash

# The wifi radio on this board can operate both in station (wifi) mode and AP mode.
# The catch is both modes must operate on the same wifi channel.
# This script selectes the best band and channel for the new access point, using 
# either the previous wifi connection's channel or the channel of the access point
# with the strongest signal (which is the AP most likely to be used later).
# It then creates the interface later used by hostapd.

list_aps() {
    # `scan` just refreshes the list and normally returns 'OK", so discard it
    wpa_cli -iwlan0 scan > /dev/null
    # `scan_results` is the output we want
    wpa_cli -iwlan0 scan_results | grep : | awk 'BEGIN {FS=" "; OFS=",";}{channel = ($2<2472) ? ($2-2407)/5 : (($2<5745) ? 32+($2-5160)/5 : 149+($2-5745)/5 ); print $3,channel,$5}' | sort
}

# get channel of previous AP's SSID 
OLD_CHANNEL=$(grep channel /etc/hostapd/hostapd.conf | cut -d '=' -f2)
# get previous wifi connection's SSID name
OLD_SSID=$(grep -i ssid /etc/wpa_supplicant/wpa_supplicant.conf | cut -d '=' -f 2 | sed 's/"//g;s/^ *//;s/ *$//;')
# get the channel currently used by the SSID
CURRENT_CHANNEL=$(list_aps | awk -F ',' "/$OLD_SSID/"' {print $2}')
logger -t "$0" "OLD_SSID = ${OLD_SSID}, OLD_CHANNEL = ${OLD_CHANNEL}, CURRENT_CHANNEL = ${CURRENT_CHANNEL}"
# get the channel of the strongest connection
if [ ! "$CURRENT_CHANNEL" ]
then
    # get channel of strongest signal
    NEW_CHANNEL=$(list_aps | head -n 1 | cut -d ',' -f 2)
    NEW_SSID=$(list_aps | head -n 1 | cut -d ',' -f 3)
    logger -t "$0" "SSID unavailable, using strongest channel (${NEW_CHANNEL}) of \"${NEW_SSID}\" for hotspot channel"
else
    NEW_CHANNEL="$CURRENT_CHANNEL"
    logger -t "$0" "using channel ${NEW_CHANNEL} used by \"${OLD_SSID}\" for hotspot channel"
fi
# updated hostapd.conf if needed
if [ "$NEW_CHANNEL" -gt 14 ]
then
    NEW_BAND=a
else
    NEW_BAND=g
fi
logger -t "$0" "setting new AP channel to ${NEW_CHANNEL} and band to ${NEW_BAND}"
sed -i "/channel=/ s/=.*/=${NEW_CHANNEL}/" /etc/hostapd/hostapd.conf
sed -i "/hw_mode=/ s/=.*/=${NEW_BAND}/" /etc/hostapd/hostapd.conf
if [ ! -e /sys/class/net/ap0 ]
then
    iw dev wlan0 interface add ap0 type __ap
fi
