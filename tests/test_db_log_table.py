#! /home/jim/venv/aiohttp/bin/python3  

# This program tests the response of the update_local_db function.
# Make a copy of the server directory first so as to not effect
#   current database values, then move this script to the main app
#   directory.
# Change the data and generator threshold values below to see 
#   what effect it has on stored database values
import asyncio
from server import update_local_db
data = {'volts':13, 'battery': -1.5, 'socket_1': 1.5, 'socket_2':0, 'socket_3':0, 'socket_4':0, 'socket_5':0, 'socket_6':0, 'fan':0, 'generator':6, 'email':'jim@pedalpc.com'}
GENERATOR_THRESHOLD = 3
asyncio.get_event_loop().run_until_complete(update_local_db(data,GENERATOR_THRESHOLD))


